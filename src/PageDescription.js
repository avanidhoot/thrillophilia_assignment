import React, { Component } from 'react';


class PageDescription extends Component {
    constructor(props) {
        super(props);
        this.state = {showing: false, display: true};
    }

    handleClose = () => this.setState({showing: false, display: true})
    handleOpen = () => this.setState({showing: true, display: false})

    render() {
        return (
            <div style={{fontSize:'16px',
                lineHeight: '1.5em',marginTop:'40px',
                height: '408px',textAlign:'left',padding:'0px 100px', fontFamily:  "Arial\", sans-serif"}}>
                <div >
                    Bangalore, also known as Bengaluru is the capital city of the Indian state Karnataka. It is the 5th
                    largest city and is known as the Garden City of India. Due to the large number of technology
                    companies established in the city, it is often referred to as the ‘Silicon Valley’ of India.The
                    modern city of Bangalore was founded over 400 years ago and gained prominence in the 18th century,
                    when it became an important fort city under Hyder Ali and Tipu Sultan of Mysore. Bangalore has many
                    attractions and is perfect for expeditions and day trips to nearby hill stations. There are a number
                    of best places to visit in Bangalore, which include Lalbagh Garden, Cubbon Park, Tipu Sultan's
                    Palace, Bangalore Palace, Vishweshwaraiah Museum,
                </div>
                {this.state.display ?
                    <div style={{color: '#f16f30'}} onClick={this.handleOpen}>
                        Show More
                    </div> : null}
                {this.state.showing ?
                    <div>
                        <div>
                            HAL Aerospace Museum, Bull Temple, Nandi Hills, Bannerghatta National Park, Vidhana Soudha
                            and ISKCON temple. All these places are a beautiful blend of palaces, gardens, museums,
                            temples and manmade beauty; so you can choose to visit the best ones according to your
                            preference and choice.
                        </div>
                        <div style={{marginTop:'10px'}}>
                            Bangalore Airport is located about 36 km from the City junction and is extremely
                            well-connected with major cities like Kolkata, Mumbai, Delhi, Hyderabad, Chennai, Ahmedabad,
                            Goa, Kochi, Mangalore, Pune and Thiruvananthapuram. The city has two important railway
                            stations - Bengaluru City Railway Station and Yesvantpur Junction, both of which have
                            regular trains from most parts of India. Bangalore is a neat, tidy and well maintained city.
                            The roads are wide and in good condition. The climate in Bangalore is very pleasant most
                            times of the year. People of this city speak Kannada which is their first language and
                            English which is the formal language. On your trip to this city, you will come across many
                            coffee shops as Bangaloreans are very fond of it. Moreover, do try eating ‘raggimudda’ which
                            is a local food item, favourite of most Kannada people. The city enjoys it, so will you! So
                            check out the best places to visit in Bangalore and have an amazing getaway vacation here.
                        </div>
                        <div style={{color: '#f16f30'}} onClick={this.handleClose}>
                            Show Less
                        </div>
                    </div> : null}
            </div>
        )
    }
}
export default PageDescription;
