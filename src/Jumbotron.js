import React, { Component } from 'react';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import {Icon,Breadcrumb} from "semantic-ui-react";
const sections = [
    { key: 'Home', content: 'Home', link: true },
    { key: 'India', content: 'India', link: true },
    { key: 'Karnataka', content: 'Karnataka', link: true },
    { key: 'Banglore', content: 'Banglore', active: true },

]
class Jumbotron extends Component {
render(){
    return(
        <div>
            <div className={'row'} style={{boxSizing:'border-box'}}>
                <div className={'col-md-12 col-sm-12 col-xs-12'}>
                    <div style={{fontSize: '16px'}}>
                        <div className={'col-md-4 col-md-offset-1 col-sm-4'}  style={{float: 'left',
                            textAlign: 'left',marginLeft:'20px',marginTop:'15px'}}>
                            <p style={{marginLeft:'40px'}}>User Reviews</p>
                            <p style={{marginLeft:'40px'}}>
                                <Icon name={'star'} color={'yellow'}/>
                                <Icon name={'star'} color={'yellow'}/>
                                <Icon name={'star'} color={'yellow'}/>
                                <Icon name={'star'} color={'yellow'}/>
                                <Icon name={'star'} color={'grey'}/>
                                <text style={{fontSize: '16px',color:'#f16f30'}}>1426 Reviews</text>
                            </p>
                        </div>
                        <div className={'col-md-7 col-sm-7'} style={{float: 'right',marginTop:'15px',
                            textAlign: 'right'}}>
                            <p style={{marginRight:'100px'}}>Top Trusted Travel Brand</p>
                            <p>
                                <ul style={{listStyleType:'none',display: 'inline',marginRight:'100px'}}>
                                    <li style={{display:'inline',marginRight:'5px'}}>
                                        <img src={require('./common/images/adtravel.png')} style={{color:'grey',height:'35px'}}/>
                                    </li>
                                    <li style={{display:'inline',marginRight:'5px'}}>
                                        <img src={require('./common/images/cnbc.png')} style={{color:'grey',height:'30px'}}/>
                                    </li>
                                    <li style={{display:'inline',marginRight:'5px'}}>
                                        <img src={require('./common/images/cnn.jpeg')} style={{color:'grey',height:'30px'}}/>
                                    </li>
                                    <li style={{display:'inline',marginRight:'5px'}}>
                                        <img src={require('./common/images/forbes.png')} style={{color:'grey',height:'25px'}}/>
                                    </li>
                                </ul>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div className={'col-md-12 col-sm-12 col-xs-12'}>
                <ul style={{listStyleType:'none', float:'left', display: 'inline',paddingLeft: '61px',
                    marginBottom: '0px'}}>
                    <li style={{display:'inline',marginRight:'5px'}}>
                        <a href={'/'} style={{color:'#b5bac0'}}> Home ></a>
                    </li>
                    <li style={{display:'inline',marginRight:'5px'}}>
                        <a href={'/'} style={{color:'#b5bac0'}}> India ></a>
                    </li>
                    <li style={{display:'inline',marginRight:'5px'}}>
                        <a href={'/'} style={{color:'#b5bac0'}}> Karantaka ></a>
                    </li>
                    <li style={{display:'inline',marginRight:'5px'}}>
                        <a href={'/'} style={{color:'#f16f30'}}> Banglore </a>
                    </li>
                </ul>
            </div>
            {/*    <Breadcrumb icon='right angle' sections={sections} />*/}
        </div>
    )
}
}
export default Jumbotron;