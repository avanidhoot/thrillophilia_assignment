import React, { Component } from 'react';
import './App.css';
import {Icon} from "semantic-ui-react";

class Header extends Component {

    render() {
        return (
            <div style={{paddingLeft: '20px',paddingRight: '20px',paddingTop:'10px',
                width: '100%'}}>
                <div>
                    <a href={"/"}>
                        <img src={require('./common/images/logo.png')}
                             style={{paddingTop: '10px',float:'left',width:'150px'}}
                             alt={'logo'}/>
                    </a>
                    <nav className="navbar navbar-expand-lg navbar-light">
                        <div className="navbar-nav col-8" id="navbarNav" style={{paddingLeft:'20px', color:'white'}}>
                            <ul className="nav navbar-nav pull_left">
                                <li className="nav-item active">
                                    <a className="nav-link" style={{color:'white'}}href="/">Gateways <span className="sr-only">(current)</span></a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" style={{color:'white'}}href="/">Tours</a>
                                </li>
                                <li className="nav-item"  style={{marginRight:'8px'}}>
                                    <a className="nav-link"style={{color:'white'}} href="/">Activities</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" style={{color:'white'}} href="/">
                                       <Icon name={'search'} />
                                        Search</a>
                                </li>
                            </ul>
                        </div>
                        <div className="navbar-nav col-4" id="navbarNav" style={{textAlign:'right'}}>
                            <ul className="nav navbar-nav navbar-right">
                                <li className="nav-item " style={{marginRight:'3px'}}>
                                    <a className="nav-link" style={{color:'white'}} href="/">
                                        <Icon name={'gift'} />
                                        Gifting An Experience <span className="sr-only">(current)</span></a>
                                </li>
                                <li className="nav-item "  style={{marginRight:'4px'}}>
                                    <a className="nav-link" style={{color:'white'}} href="/">
                                        <Icon name={'mobile alternate'} />
                                        Get App <span className="sr-only">(current)</span></a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="/" style={{
                                        border: '1px solid #f8ad33',
                                        borderRadius: '7px',
                                        width: '74px',
                                        marginTop:'04px',
                                        padding:'5px',
                                        height: '30px',
                                        textAlign: 'center',
                                        fontSize: '13px',position:'relative',
                                        backgroundColor:'#f8ad33',
                                        color: '#fff'}}>
                                        Login</a>
                                </li>
                            </ul>

                    </div>
                </nav>
                </div>
            </div>
        );
    }
}

export default Header;
