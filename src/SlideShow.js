import React, { Component } from 'react';
import './App.css';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import { Carousel } from 'react-responsive-carousel';
import Header from "./Header";
class SlideShow extends Component {
    render(){
        return(
<div>
    <Carousel showIndicators={false}
              showStatus ={false}
              autoPlay = {true}
              showThumbs={false}
              infiniteLoop={true}>
        <div style={{
            height:'600px',
            backgroundSize: 'cover',
            backgroundImage:" url("+require('./common/images/banner-image-rentals.jpg')+")"
        }}>
            <Header/>
            <div style={{color:'white', float:'left',paddingLeft:'40px',marginTop:'260px'}}>
                <h2 className="" style={{textAlign:'left'}}>Thrillophilia Rentals</h2>
                <p className="" style={{textAlign:'left'}}>Add local flavor to your  journey, rent out bikes, cameras, gears and more!</p>
                <button style={{
                    float:'left',
                    paddingLeft:'15px',paddingRight:'15px',fontSize: '15px',
                    backgroundColor: 'transparent',
                    border: '2px solid #fff',
                    padding: '5px 30px',
                    position: 'relative',
                    color: '#fff',
                    textTransform: 'uppercase',
                    marginTop: '5px'}}>
                    BOOK RENTALS</button>
            </div>
        </div>
        <div style={{
            height:'600px',
            backgroundSize: 'cover',
            backgroundImage:" url("+require('./common/images/banner-image-events.jpg')+")"
        }}>
            <Header/>
            <div style={{color:'white', float:'left',paddingLeft:'40px',marginTop:'260px'}}>
                <h2 className=""style={{textAlign:'left'}}>Thrillophilia Events</h2>
                <p className="" style={{textAlign:'left'}}>What's Happening Now?</p>
                <button style={{
                    float:'left',
                    paddingLeft:'15px',paddingRight:'15px',fontSize: '15px',
                    backgroundColor: 'transparent',
                    border: '2px solid #fff',
                    padding: '5px 30px',
                    position: 'relative',
                    color: '#fff',
                    textTransform: 'uppercase',
                    marginTop: '5px'}}>EXPLORE EVENTS</button>

            </div>
        </div>
        <div style={{
            height:'600px',
            backgroundSize: 'cover',
            backgroundImage:" url("+require('./common/images/banner-image-attraction.jpg')+")"
        }}>
            <Header/>
            <div style={{color:'white', float:'left',paddingLeft:'40px',marginTop:'260px'}}>
                <h2 className="" style={{textAlign:'left'}}>Best Attractions of Bangalore</h2>
                <p className="" style={{textAlign:'left'}}>Do not forget to check out our handpicked list of attractions in Bangalore</p>
                <button style={{
                    float:'left',
                    paddingLeft:'15px',paddingRight:'15px',fontSize: '15px',
                    backgroundColor: 'transparent',
                    border: '2px solid #fff',
                    padding: '5px 30px',
                    position: 'relative',
                    color: '#fff',
                    textTransform: 'uppercase',
                    marginTop: '5px'}}>WHAT TO SEE?</button>
            </div>
        </div>
        <div style={{
            height:'600px',
            backgroundSize: 'cover',
            backgroundImage:" url("+require('./common/images/banner-image-stays.jpg')+")"
        }}>
            <Header/>
            <div style={{color:'white', float:'left',paddingLeft:'40px',marginTop:'260px'}}>
                <h2 className=""style={{textAlign:'left'}}>Thrillophilia Staycations</h2>
                <p className=""style={{textAlign:'left'}}>Our handpicked most experiential stays in Bangalore</p>
                <button style={{
                    float:'left',
                    paddingLeft:'15px',paddingRight:'15px',fontSize: '15px',
                    backgroundColor: 'transparent',
                    border: '2px solid #fff',
                    padding: '5px 30px',
                    position: 'relative',
                    color: '#fff',
                    textTransform: 'uppercase',
                    marginTop: '5px'}}>EXPLORE STAYCATIONS</button>

            </div>
        </div>
        <div style={{
            height:'600px',
            backgroundSize: 'cover',
            backgroundImage:" url("+require('./common/images/banner-image-things-to-do.jpg')+")"
        }}>
            <Header/>
            <div style={{color:'white', float:'left',paddingLeft:'40px',marginTop:'260px'}}>
                <h2 className="" style={{textAlign:'left'}}>Thrillophilia Activities</h2>
                <p className=""style={{textAlign:'left'}}>Find best things to do and experience more!</p>
                <button style={{
                    float:'left',
                    paddingLeft:'15px',paddingRight:'15px',fontSize: '15px',
                    backgroundColor: 'transparent',
                    border: '2px solid #fff',
                    padding: '5px 30px',
                    position: 'relative',
                    color: '#fff',
                    textTransform: 'uppercase',
                    marginTop: '5px'}}>SEE ALL ACTIVITIES</button>

            </div>

        </div>
        <div style={{
            height:'600px',
            backgroundSize: 'cover',
            backgroundImage:" url("+require('./common/images/banner-image-tours-.jpg')+")"
        }}>
            <Header/>
            <div style={{color:'white', float:'left',paddingLeft:'40px',marginTop:'260px'}}>
                <h2 className=""style={{textAlign:'left'}} >Thrillophilia Tours</h2>
                <p className=""style={{textAlign:'left'}}>Experiential journeys will make you a storyteller</p>

                <button style={{
                    float:'left',
                    paddingLeft:'15px',paddingRight:'15px',fontSize: '15px',
                    backgroundColor: 'transparent',
                    border: '2px solid #fff',
                    padding: '5px 30px',
                    position: 'relative',
                    color: '#fff',
                    textTransform: 'uppercase',
                    marginTop: '5px'}}>SHOW TOURS</button>
            </div>
        </div>
    </Carousel>
</div>
        )
    }
}
export default SlideShow;
