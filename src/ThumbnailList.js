import React, { Component } from 'react';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import {Card,Image,Button} from "semantic-ui-react";

class ThumbnailList extends Component {
    render(){
        return(
            <div style={{textAlign:'left',marginLeft:'40px',marginRight:'40px'}}>
                <h2 style={{textAlign:'left'}}>Camping Around Bangalore - Upto 30% OFF</h2>
                <div style={{}}>
                    <Card.Group>
                    <Card>
                        <Card.Content>
                            <Image size='mini' src='/images/avatar/large/steve.jpg' />
                            <Card.Header>Steve Sanders</Card.Header>
                            <Card.Meta>Friends of Elliot</Card.Meta>
                            <Card.Description>
                                Steve wants to add you to the group <strong>best friends</strong>
                            </Card.Description>
                        </Card.Content>

                    </Card>
                        <Card>
                            <Card.Content>
                                <Image  size='mini' src='/images/avatar/large/steve.jpg' />
                                <Card.Header>Steve Sanders</Card.Header>
                                <Card.Meta>Friends of Elliot</Card.Meta>
                                <Card.Description>
                                    Steve wants to add you to the group <strong>best friends</strong>
                                </Card.Description>
                            </Card.Content>
                        </Card>
                    <Card>
                        <Card.Content>
                            <Image size='mini' src='/images/avatar/large/molly.png' />
                            <Card.Header>Molly Thomas</Card.Header>
                            <Card.Meta>New User</Card.Meta>
                            <Card.Description>
                                Molly wants to add you to the group <strong>musicians</strong>
                            </Card.Description>
                        </Card.Content>
                        <Card.Content extra>
                            <div className='ui two buttons'>
                                <Button basic color='green'>
                                    Approve
                                </Button>
                                <Button basic color='red'>
                                    Decline
                                </Button>
                            </div>
                        </Card.Content>
                    </Card>
                    <Card>
                        <Card.Content>
                            <Image floated='right' size='mini' src='/images/avatar/large/jenny.jpg' />
                            <Card.Header>Jenny Lawrence</Card.Header>
                            <Card.Meta>New User</Card.Meta>
                            <Card.Description>Jenny requested permission to view your contact details</Card.Description>
                        </Card.Content>
                        <Card.Content extra>
                            <div className='ui two buttons'>
                                <Button basic color='green'>
                                    Approve
                                </Button>
                                <Button basic color='red'>
                                    Decline
                                </Button>
                            </div>
                        </Card.Content>
                    </Card>
                </Card.Group>
                </div>
            </div>
        )

    }
}
export default ThumbnailList;