import React, { Component } from 'react';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import SlideShow from "./SlideShow";
import Jumbotron from "./Jumbotron";
import PageDescription from "./PageDescription";
import ThumbnailList from "./ThumbnailList";

class HomePage extends Component {
    render() {

        return (
            <div>
                <div>
                    <SlideShow/>
                </div>
                <div>
                    <Jumbotron/>
                </div>
                <div>
                    <PageDescription/>
                </div>
                <div>
                    <ThumbnailList/>
                </div>
            </div>
        );
    }
}

export default HomePage;
